
document.querySelector('.theme').addEventListener('click', () => {
  document.body.classList.toggle('dark')
  if (localStorage.getItem('dark')) {
    localStorage.removeItem('dark')
  } else {
    localStorage.setItem('dark', 'true')
  }
})

if (localStorage.getItem('dark')) {
  document.body.classList.add('dark')
} 